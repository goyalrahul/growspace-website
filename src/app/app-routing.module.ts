import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { DealerManagementComponent } from './dealer-management/dealer-management.component';
import { InventoryComponent } from './inventory/inventory.component';
import { KycComponent } from './kyc/kyc.component';
import { OrderComponent } from './order/order.component';
import { ReportsComponent } from './reports/reports.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { SimManagementComponent } from './sim-management/sim-management.component';
import { SimSearchComponent } from './sim-search/sim-search.component';
import { SupportComponent } from './support/support.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { WhitelistingComponent } from './whitelisting/whitelisting.component';
import { AddUserComponent } from './add-user/add-user.component';
import { OrganizationComponent } from './organization/organization.component';
import { RequestComponent } from './request/request.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { SeeDetailsSimComponent } from './see-details-sim/see-details-sim.component';

const routes: Routes = [
  {path: "", component: DashboardComponent},
  {path: "dashboard", component: DashboardComponent},
  {path: "order", component: OrderComponent},
  {path: "reports", component: ReportsComponent},
  {path: "inventory", component: InventoryComponent},
  {path: "sim-management", component: SimManagementComponent},
  {path: "dealer-management", component: DealerManagementComponent},
  {path: "user-management", component: UserManagementComponent},
  {path: "support", component: SupportComponent},
  {path: "sim-search", component: SimSearchComponent},
  {path: "kyc", component: KycComponent},
  {path: "whitelisting", component: WhitelistingComponent},
  {path: "add-user", component: AddUserComponent},
  {path: "organization", component: OrganizationComponent},
  {path: "request", component: RequestComponent},
  {path: "delete-modal", component: DeleteModalComponent},
  {path: "see-details-sim", component: SeeDetailsSimComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
