import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { CommonModule, DatePipe } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { OrderComponent } from './order/order.component';
import { ReportsComponent } from './reports/reports.component';
import { InventoryComponent } from './inventory/inventory.component';
import { SimManagementComponent } from './sim-management/sim-management.component';
import { DealerManagementComponent } from './dealer-management/dealer-management.component';
import { UserManagementComponent } from './user-management/user-management.component';
import { SupportComponent } from './support/support.component';
import { SimSearchComponent } from './sim-search/sim-search.component';
import { KycComponent } from './kyc/kyc.component';
import { WhitelistingComponent } from './whitelisting/whitelisting.component';
import { DirectiveModule } from './directives/directives.module';
import { AddUserComponent } from './add-user/add-user.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { OrganizationComponent } from './organization/organization.component';
import { RequestComponent } from './request/request.component';
import { DeleteModalComponent } from './delete-modal/delete-modal.component';
import { SeeDetailsSimComponent } from './see-details-sim/see-details-sim.component';
import { BreadcrumbComponent } from './breadcrumb/breadcrumb.component';

@NgModule({
  declarations: [
    AppComponent,
    SidebarComponent,
    HeaderComponent,
    DashboardComponent,
    OrderComponent,
    ReportsComponent,
    InventoryComponent,
    SimManagementComponent,
    DealerManagementComponent,
    UserManagementComponent,
    SupportComponent,
    SimSearchComponent,
    KycComponent,
    WhitelistingComponent,
    AddUserComponent,
    OrganizationComponent,
    RequestComponent,
    DeleteModalComponent,
    SeeDetailsSimComponent,
    BreadcrumbComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    DirectiveModule,
    NgbModule,
    CommonModule
  ],
  providers: [
    DatePipe
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
