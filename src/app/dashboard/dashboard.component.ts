import { Component, OnInit } from '@angular/core';
import { right } from '@popperjs/core';
import { Chart, registerables } from 'chart.js';
Chart.register(...registerables);

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {

  constructor() {
  }

  createPieChart() {
    var myPieChart = new Chart("myPieChart", {
      type: 'pie',
      data: {
        labels: ['Operational eSIM          60', 'eSIM          20', 'Suspended eSIM          20'],
        datasets: [
          {
            label: '',
            data: [60, 20, 20],
            backgroundColor: [
              '#4017f1',
              '#64c8f3',
              '#f1f5ff'
            ],
            borderColor: [
              '#4017f1',
              '#64c8f3',
              '#f1f5ff' 
            ],
            borderWidth: 1
          }
        ]
      },
      options:
      {
        scales: {
          x: {
            display:false,
            grid: {
              display: false
            }
          },
          y: {
            display:false,
            grid: {
              display: false
            }
          }
        },
        plugins: {
          legend: {
            display: true,
            position: 'right',
            labels:{
              padding: 20,
              textAlign: right,
              font:{
                size: 10,
              },
              boxWidth:12,
              boxPadding:6,
              borderRadius:4,
            }
          }
        },
        responsive: true,
        maintainAspectRatio: false,
      }
    });
  }


  createLineChart() {
    var myLineChart = new Chart("myLineChart", {
      type: 'line',
      data: {
        labels: ['SEP', 'OCT', 'NOV', 'DEC', 'JAN', 'FEB'],
        datasets: [
          {
            label: '',
            data: [70, 60, 80, 50, 80, 85],
            tension:0.5,
            backgroundColor: '#4017f1',
            borderColor:'#4017f1',
            pointBorderColor: '#4017f1',
            pointBackgroundColor: '#ffffff',
            pointBorderWidth: 5,
            // backgroundColor: [
            //   '#4017f1',
            //   '#64c8f3',
            //   '#f1f5ff'
            // ],
            // borderColor: [
            //   '#4017f1',
            //   '#64c8f3',
            //   '#f1f5ff' 
            // ],
            borderWidth: 2
          },
          {
            label: '',
            data: [40, 30, 45, 25, 40, 50],
            tension:0.5,
            backgroundColor: '#64c8f3',
            borderColor:'#64c8f3',
            pointBorderColor: '#64c8f3',
            pointBackgroundColor: '#ffffff',
            pointBorderWidth: 5,
            // backgroundColor: [
            //   '#4017f1',
            //   '#64c8f3',
            //   '#f1f5ff'
            // ],
            // borderColor: [
            //   '#4017f1',
            //   '#64c8f3',
            //   '#f1f5ff' 
            // ],
            borderWidth: 2
          }
        ]
      },
      options:
      {
        elements: {
          point: {
            radius:0,
          }
        },
        hover: {
          intersect: false,
        },
        scales: {
          x: {
            display:true,
            beginAtZero:true,
            grid: {
              display: false
            },
            ticks: {
              font: {
                size: 9,
              }
            },
          },
          y: {
            display:false,
            beginAtZero:true,
            grid: {
              display: false
            }
          }
        },
        plugins: {
          legend: {
            display: false,
            position: 'right',
            labels:{
              padding: 20,
              textAlign: right,
              font:{
                size: 10,
              },
              boxWidth:12,
              boxPadding:6,
              borderRadius:4,
            }
          }
        },
        responsive: true,
        maintainAspectRatio: false,
      }
    });
  }

  
  ngOnInit(): void {
    this.createPieChart();
    this.createLineChart();
  }

}
