import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddUserComponent } from '../add-user/add-user.component';

@Component({
  selector: 'app-organization',
  templateUrl: './organization.component.html',
  styleUrls: ['./organization.component.scss']
})
export class OrganizationComponent implements OnInit {

  @ViewChild('editcontent') edittemplateRef: TemplateRef<any>;
  
  data :any= [];
  table = {
    data: {
      headings: {},
      columns: []
    },
    settings: {
      hideHeader: true
    }
  };

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    this.setTable();
  }

  resetTable() {
    this.table.data = {
      headings: {},
      columns: []
    };
  }

  setTable() {
    this.data=[
      {"Name":'Rahul Goyal', "Type":"Corporate Account", "GST Number":"18NEBCU9703R1ZM", "Users":"12", "Action":null},
      {"Name":'Ankit Rawat', "Type":"Channel Partner", "GST Number":"25RGBCU9703R1ZM", "Users":"5", "Action":null},
      {"Name":'Yash Vijay', "Type":"Corporate Account", "GST Number":"12NEBCU9703R1ZM", "Users":"4", "Action":null}
    ]
    this.table.data = {
      headings: this.generateHeadings(),
      columns: this.getTableColumns()
    };
    return true;
  }

  generateHeadings() {
    let headings = {}; 
    for (var key in this.data[0]) {
      if (key.charAt(0) != "_") {
        headings[key] = { title: key, placeholder: (key) };
      }
    }
    return headings;
  }

  getTableColumns() {
    let columns = [];
    this.data.map(task => {
      let column = {};
      for (let key in this.generateHeadings()) {
        if (key.toLowerCase() == 'action') {
          column[key] = {
            value: "",
            isHTML: true,
            action: null,
            icons: this.actionIcons(task)
          };
        } 
        else{
          column[key] = { value: task[key], class: 'black', action: '' };
        }
      }
      columns.push(column);
    });
    return columns;
  }

  actionIcons(item) {
    let icons = [
      { class:"fa fa-plus", action: this.getAdd.bind(this,item),title:"Add"},
      { class:"fa fa-edit", action: this.getEdit.bind(this,item),title:"Edit"},
     ];
    return icons;
  }

  getAdd(row) {
		this.modalService.open(AddUserComponent, { size: 'lg' });
	}

  getEdit(row){
    this.modalService.open(this.edittemplateRef, { size: 'lg' });
  }

}
