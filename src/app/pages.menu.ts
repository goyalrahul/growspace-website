export interface sidebarMenus {
    // title: string,
    // img: string,
    // url: any,
    // children:any,
    // flag:any
}

export const sidebar: sidebarMenus[] = [
    {
        title: 'Dashboard',
        img: 'assets/images/dashboard.svg',
        url: 'dashboard'
    },
    {
        title: 'Reports',
        img: 'assets/images/report.svg',
        url: 'reports'
    },
    {
        title: 'User Management',
        img: 'assets/images/user_management.svg',
        url: 'user-management'
    },
    {
        title: 'Organization',
        img: 'assets/images/organization.svg',
        url: 'organization'
    },
    {
        title: 'Inventory',
        img: 'assets/images/inventory.svg',
        url: 'inventory'
    },
    {
        title: 'Sim Management',
        img: 'assets/images/sim_management.svg',
        url: 'sim-management',
    },

    // {
    //     title: 'Sim Management',
    //     img: 'assets/images/sim_management.svg',
    //     url: null,
    //     flag: false,
    //     children:[
    //         {
    //             title: 'Sim Search',
    //             img: 'assets/images/search_menu.svg',
    //             url: 'sim-search'
    //         },
    //         {
    //             title: 'KYC',
    //             img: 'assets/images/kyc_menu.svg',
    //             url: 'kyc'
    //         },
    //         {
    //             title: 'WhiteListing',
    //             img: 'assets/images/whitelist_menu.svg',
    //             url: 'whitelisting'
    //         }
    //     ]
    // },

    {
        title: 'Request',
        img: 'assets/images/support.svg',
        url: 'request'
    },
    {
        title: 'Order',
        img: 'assets/images/order.svg',
        url: 'order'
    },
    {
        title: 'Dealer Management',
        img: 'assets/images/dealer_management.svg',
        url: 'dealer-management'
    },
    {
        title: 'Support',
        img: 'assets/images/support.svg',
        url: 'support'
    }
]