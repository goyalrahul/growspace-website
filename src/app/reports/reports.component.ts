import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit {
  @ViewChild('editcontent') edittemplateRef: TemplateRef<any>;
  
  data :any= [];
  table = {
    data: {
      headings: {},
      columns: []
    },
    settings: {
      hideHeader: true
    }
  };

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
    this.setTable();
  }

  resetTable() {
    this.table.data = {
      headings: {},
      columns: []
    };
  }

  setTable() {
    this.data=[
      {"Order":"Abc", "Iccid":'8991102105454892', "Profile":"Airtel+BSNL", "Activation Data":"24 Jan 2021", "Expiry Date":"24 Jan 2021", "Status":"Operational", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Action":null},
      {"Order":"Asd", "Iccid":'8991102105454892', "Profile":"Airtel+BSNL", "Activation Data":"12 Jun 2021", "Expiry Date":"12 Jun 2021", "Status":"Bootstarp", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Action":null},
      {"Order":"Xyz", "Iccid":'8991102105454892', "Profile":"Voda+BSNL", "Activation Data":"6 Jan 2021", "Expiry Date":"6 Jan 2021", "Status":"Operational", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Action":null},
      {"Order":"Rsp", "Iccid":'8991102105454892', "Profile":"Voda+BSNL", "Activation Data":"7 Mar 2021", "Expiry Date":"7 Mar 2021", "Status":"Suspended", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Action":null},
      {"Order":"Tuv", "Iccid":'8991102105454892', "Profile":"Airtel+BSNL", "Activation Data":"17 Dec 2021", "Expiry Date":"17 Dec 2021", "Status":"Operational", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Action":null}
    ]
    this.table.data = {
      headings: this.generateHeadings(),
      columns: this.getTableColumns()
    };
    return true;
  }

  generateHeadings() {
    let headings = {}; 
    for (var key in this.data[0]) {
      if (key.charAt(0) != "_") {
        headings[key] = { title: key, placeholder: (key) };
      }
    }
    return headings;
  }
  
  getTableColumns() {
    let columns = [];
    this.data.map(task => {
      let column = {};
      for (let key in this.generateHeadings()) {
        if (key.toLowerCase() == 'action') {
          column[key] = {
            value: "",
            isHTML: true,
            action: null,
            icons: this.actionIcons(task)
          };
        } 
        else{
          column[key] = { value: task[key], class: 'black', action: '' };
        }
      }
      columns.push(column);
    });
    return columns;
  }
  
  actionIcons(item) {
    let icons = [
      { class:"fa fa-edit", action: this.getEdit.bind(this,item),title:"Edit"},
     ];
    return icons;
  }

  getEdit(row){
    this.modalService.open(this.edittemplateRef, { size: 'lg' });
  }

}
