import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SeeDetailsSimComponent } from './see-details-sim.component';

describe('SeeDetailsSimComponent', () => {
  let component: SeeDetailsSimComponent;
  let fixture: ComponentFixture<SeeDetailsSimComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SeeDetailsSimComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SeeDetailsSimComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
