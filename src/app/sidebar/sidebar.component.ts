import { Component, OnInit } from '@angular/core';
import { sidebar } from '../pages.menu';
import { CommonService } from '../services/common.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  sidebarMenus: any[] = [];
  activeIndex: number = -1;
  constructor(public common: CommonService) {
    document.body.classList.add('expanded');
    this.sidebarMenus = sidebar;
  }

  menuClick(index: number) {
    if(this.activeIndex !== index){
      this.sidebarMenus.forEach(item => item.flag = false);
    }
    this.activeIndex = index;
    this.sidebarMenus[index].flag =  !this.sidebarMenus[index].flag;
  }

  toggleSidebar() {
    this.common.expanded = !this.common.expanded
    if (this.common.expanded) {
      document.body.classList.add('expanded');
      document.body.classList.remove('collapsed');
    }

    else {
      document.body.classList.add('collapsed');
      document.body.classList.remove('expanded');
    }
  }

  ngOnInit(): void {
  }
}

