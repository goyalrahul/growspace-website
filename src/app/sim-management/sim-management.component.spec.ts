import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimManagementComponent } from './sim-management.component';

describe('SimManagementComponent', () => {
  let component: SimManagementComponent;
  let fixture: ComponentFixture<SimManagementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimManagementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimManagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
