import { Component, OnInit} from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sim-management',
  templateUrl: './sim-management.component.html',
  styleUrls: ['./sim-management.component.scss']
})
export class SimManagementComponent implements OnInit {

  selectedIp: boolean = false;
  selectedNumber: boolean = false;
  minusIcon: boolean = false;

  allWhitelistIP = [
    {name: ''}
  ]

  allWhitelistNumber = [
    {name: ''}
  ]
  
  data :any= [];
  table = {
    data: {
      headings: {},
      columns: []
    },
    settings: {
      hideHeader: true
    }
  };

  constructor(private modalService: NgbModal, private router: Router) { }

  ngOnInit(): void {
    this.setTable();
  }

  resetTable() {
    this.table.data = {
      headings: {},
      columns: []
    };
  }

  setTable() {
    this.data=[
      {"Select":"", "Order":"Abc", "Iccid":'8991102105454892', "Profile":"Airtel+BSNL", "Activation Data":"24 Jan 2021", "Expiry Date":"24 Jan 2021", "Status":"Operational", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Information":"Activation", "Action":null},
      {"Select":"", "Order":"Asd", "Iccid":'8991102105454892', "Profile":"Airtel+BSNL", "Activation Data":"12 Jun 2021", "Expiry Date":"12 Jun 2021", "Status":"Bootstarp", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Information":"Pending", "Action":null},
      {"Select":"", "Order":"Xyz", "Iccid":'8991102105454892', "Profile":"Voda+BSNL", "Activation Data":"6 Jan 2021", "Expiry Date":"6 Jan 2021", "Status":"Operational", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Information":"Pending", "Action":null},
      {"Select":"", "Order":"Rsp", "Iccid":'8991102105454892', "Profile":"Voda+BSNL", "Activation Data":"7 Mar 2021", "Expiry Date":"7 Mar 2021", "Status":"Suspended", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Information":"Activation", "Action":null},
      {"Select":"", "Order":"Tuv", "Iccid":'8991102105454892', "Profile":"Airtel+BSNL", "Activation Data":"17 Dec 2021", "Expiry Date":"17 Dec 2021", "Status":"Operational", "Imsi 1":"-", "Msisdn":"-", "Imsi 2":"-", "Msisdn 2":"-", "Information":"Pending", "Action":null}
    ]
    this.table.data = {
      headings: this.generateHeadings(),
      columns: this.getTableColumns()
    };
    return true;
  }

  generateHeadings() {
    let headings = {}; 
    for (var key in this.data[0]) {
      if (key.charAt(0) != "_") {
        headings[key] = { title: key, placeholder: (key) };
      }
    }
    return headings;
  }
  
  getTableColumns() {
    let columns = [];
    this.data.map(task => {
      let column = {};
      for (let key in this.generateHeadings()) {
        if (key.toLowerCase() == 'action') {
          column[key] = {
            value: "",
            isHTML: true,
            action: null,
            icons: this.actionIcons(task)
          };
        } 

       else if(key.toLowerCase() == 'select'){
          column[key] = {
            value: "",
            isHTML: false,
            action: null,
            isCheckbox:true,
          };
        }
        else{
          column[key] = { value: task[key], class: 'black', action: '' };
        }
      }
      columns.push(column);
    });
    return columns;
  }
  
  actionIcons(item) {
    let icons = [
      { class:"fa fa-arrow-right", action: this.getSeeMore.bind(this,item),title:"See Details"},
     ];
    return icons;
  }

  getSeeMore(row){
    this.router.navigateByUrl('/see-details-sim');
  }

  activateSimModal(activateSim) {
		this.modalService.open(activateSim, { size: 'md', backdrop:'static', keyboard:false });
	}

  whiteListModal(whiteList) {
    this.selectedIp = false;
    this.allWhitelistIP = [
      {name: ''}
    ];
    this.allWhitelistNumber = [
      {name: ''}
    ]
    this.selectedNumber = false;
    this.modalService.open(whiteList, { size: 'md', backdrop:'static', keyboard:false });
	}

  addIP(){
    let ip = {name:''};
    this.allWhitelistIP.push(ip);
  }
  removeIP(j){
    this.allWhitelistIP.splice(j,1);
  }

  addNumber(){
    let number = {name:''};
    this.allWhitelistNumber.push(number);
  }
  removeNumber(k){
    this.allWhitelistNumber.splice(k,1);
  }

}
