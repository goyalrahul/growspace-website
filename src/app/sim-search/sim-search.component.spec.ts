import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SimSearchComponent } from './sim-search.component';

describe('SimSearchComponent', () => {
  let component: SimSearchComponent;
  let fixture: ComponentFixture<SimSearchComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SimSearchComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SimSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
