import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
// import { DeleteModalComponent} from '../delete-modal/delete-modal.component';
// import { url } from 'inspector';

@Component({
  selector: 'app-user-management',
  templateUrl: './user-management.component.html',
  styleUrls: ['./user-management.component.scss']
})
export class UserManagementComponent implements OnInit {

  // @ViewChild('viewcontent') viewtemplateRef: TemplateRef<any>;
  @ViewChild('editcontent') edittemplateRef: TemplateRef<any>;

  data :any= [];
  table = {
    data: {
      headings: {},
      columns: []
    },
    settings: {
      hideHeader: true
    }
  };

  constructor(private router:Router, private modalService: NgbModal) { }

  ngOnInit(): void {
    this.setTable();
  }

  resetTable() {
    this.table.data = {
      headings: {},
      columns: []
    };
  }

  setTable() {
    this.data=[
      {"Thumbnail":"A", "_image":"../../assets/images/user_img.jpg", "Name":"Ankit Rawat", "Email":"ankit@gmail.com", "Role":"Admin", "Created Time":"24 Jan 2021", "Status":"Active", "Action":null},
      {"Thumbnail":"Y", "_image":"", "Name":"Yash Vijay", "Email":"yash@gmail.com", "Role":"Employee", "Created Time":"24 Jan 2021", "Status":"Pending Verification", "Action":null},
      {"Thumbnail":"R", "_image":"", "Name":"Rahul Goyal", "Email":"rahul@gmail.com", "Role":"Employee", "Created Time":"24 Jan 2021", "Status":"Active", "Action":null}
    ]
    this.table.data = {
      headings: this.generateHeadings(),
      columns: this.getTableColumns()
    };
    return true;
  }

  generateHeadings() {
    let headings = {}; 
    for (var key in this.data[0]) {
      if (key.charAt(0) != "_") {
        headings[key] = { title: key, placeholder: (key) };
      }
    }
    return headings;
  }
  
  getTableColumns() {
    let columns = [];
    this.data.map((task:any) => {
      let column = {};
      for (let key in this.generateHeadings()) {
        if (key.toLowerCase() == 'action') {
          column[key] = {
            value: "",
            isHTML: true,
            action: null,
            icons: this.actionIcons(task)
          };
        } 

        // thumbnail image ts code start here
        else if(key.toLowerCase() == 'thumbnail'){
            if(task['_image']){
              column[key] = { value:`<img class="thumbmail-image" src="${task['_image']}">`,isHTML:true, class: 'black', action: '' };
            }

            else{
              column[key] = { value:`<div class="thumbmail-name">${task['Thumbnail']}</div>`,isHTML:true, class: 'black', action: '' };
            }
        }
        // thumbnail image ts code end here

        else{
            column[key] = { value: task[key], class: 'black', action: '' };
        }
      }
      columns.push(column);
    });
    return columns;
  }

  actionIcons(item) {
    let icons = [
      // { class:"fa fa-eye", action: this.getView.bind(this,item),title:"View"},
      { class:"fa fa-edit", action: this.getEdit.bind(this,item),title:"Edit"},
      // { class:"fa fa-trash", action: this.getDelete.bind(this,item),title:"Delete"},
     ];
    return icons;
  }

  // getView(row) {
	// 	this.modalService.open(this.viewtemplateRef, { size: 'lg' });
	// }

  getEdit(row){
    this.modalService.open(this.edittemplateRef, { size: 'lg' });
  }

  // getDelete(row){
  //   this.modalService.open(DeleteModalComponent, { size: 'md' });
  // }

  addUser(){
    this.router.navigateByUrl('/add-user');
  }

}
